/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.plants;

/**
 *
 * @author Thanya
 */
public class Chaba extends Plants {

    private String color;
    private String petal;
    

    public void introduce() {
        System.out.println("Hello I'm Chaba");
    }

    public void introduce(String color, String petal) {
        this.color = color;
        this.petal = petal;
        System.out.println("color:" + this.color + " petal:" + this.petal);
    }

    @Override
    public void checkflowertype() {
        System.out.println("Chaba:flowering plant");
    }

    @Override
    public void checkreproductivetype() {
        System.out.println("Chaba:asexually");
    }
}
